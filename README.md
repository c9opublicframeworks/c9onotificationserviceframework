# C9ONotificationService
C9ONotificationService framework provided by Cloud9 Online LLC to manage Daily Reminders, Push Notifications and Deeplinking.

## Requirements
- Xcode 12.0 or later
- iOS 11.0 or later
- Swift 5.0 or later

## Installation
### Cocoapods
1. This framework is available through [CocoaPods](http://cocoapods.org). 
2. Include C9ONotificationService in your `Pod File`:

```
pod 'C9ONotificationService'
```

## Usage
### Initialization
1. Import the C9ONotificationService header in the App Delegate.
```swift
import C9ONotificationService
```
2. Add following code in your AppDelegate's application:didFinishLaunchingWithOptions: method for initialization.
```swift
C9ONotificationService.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
```
3. Add following methods in your AppDelegate file.
```swift
func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
    return C9ONotificationService.shared.application(application, continue: userActivity, restorationHandler: restorationHandler)
}

func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
    return C9ONotificationService.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
}

func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
    C9ONotificationService.shared.application(application, performActionFor: shortcutItem, completionHandler: completionHandler)
}
```

### Daily Reminders
```swift

// Add Reminder
C9ONotificationService.shared.addReminder(date: Date(), isOn: True/False)

// Delete Reminder
C9ONotificationService.shared.deleteReminder(reminder: reminder)

// Update Reminder
C9ONotificationService.shared.updateReminder(index: Array Index, isOn: True/False)

// Get Reminders List
C9ONotificationService.shared.reminders

```
    
## Observers
To subscribe
```swift
C9ONotificationService.shared.delegate = self
```
After subscribing, you can listen for followings:

```swift
extension AppDelegate: C9ONotificationServiceDelegate {

    func didReceiveNotificationWithURL(data:[String: String])
    func didReceiveNotificationWithMeditation(meditationID: Int)
    func didViewNotification(title: String, body: String)
    
}
```

