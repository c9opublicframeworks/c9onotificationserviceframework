Pod::Spec.new do |spec|

  spec.name                     = "C9ONotificationService"
  spec.version                  = "0.0.2"
  spec.summary                  = "C9ONotificationService framework provided by Cloud9 Online LLC to manage Daily Reminders, Push Notifications and Deeplinking."
  spec.description              = "C9ONotificationService contains to functionality to register for notifications i.e. local, remote. It also handles the input of deep linking actions and notify the user with observers."
  spec.homepage                 = "https://gitlab.com/c9opublicframeworks/c9onotificationserviceframework"
  spec.license                  = { :type => 'MIT', :text => <<-LICENSE
                                        Copyright © 2021 Cloud9 Online LLC.
                                    LICENSE
                                  }
  spec.author                   = { "Cloud9 Online" => "cloud9online@gmail.com" }
  spec.ios.deployment_target    = "11.0"
  spec.source                   = { :git => "https://gitlab.com/c9opublicframeworks/c9onotificationserviceframework.git", :tag => "#{spec.version}" }
  spec.vendored_frameworks      = "C9ONotificationService.framework"
  spec.swift_versions           = ['5.0']

  spec.dependency                 'C9OCore'
  spec.dependency                 'Firebase/Messaging'
  spec.dependency                 'Firebase/DynamicLinks'
  spec.weak_frameworks          = 'Foundation', 'SystemConfiguration'

  spec.user_target_xcconfig     = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig      = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig      = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }

end
